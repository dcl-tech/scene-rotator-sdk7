# SDK7 Example of rotating a scene

1. Be sure there is an Entity in your scene that parents, directly or indirectly, everything that should be rotated.  If this already exists, for example, it may be named '_scene' or 'scene'.

2. include the rotateScene function in your scene, outside the main function.

3. By the end of your main funciton, call the rotateScene function, using parameters are explained in the example code


# SDK6 code examples below...

```
////////////////////////
// Use one of these sections to rotate a Builder _scene 90, -90, or 180 degrees
////////////////////////

// // Rotate the scene 90 degrees counter-clockwise as seen from above
// let transformToChange = _scene.getComponent(Transform)
// transformToChange.rotate(Vector3.Up(),-90) // rotate it 90 degrees clockwise as seen from top
// transformToChange.position.x += 32  // now move it back east by its original south-north size in meters

// // Rotate the scene 90 degrees clockwise as seen from above
// let transformToChange = _scene.getComponent(Transform)
// transformToChange.rotate(Vector3.Up(),90) // rotate it 90 degrees clockwise as seen from top
// transformToChange.position.z += 32  // now move it back north by its original east-west size in meters

// // Rotate the scene 180 degrees as seen from above
// let transformToChange = _scene.getComponent(Transform)
// transformToChange.rotate(Vector3.Up(),180) // rotate it 90 degrees clockwise as seen from top
// transformToChange.position.x += 32  // now move it back east by its original east-west size in meters
// transformToChange.position.z += 32  // now move it back north by its original south-north size in meters

/**
 * rotateScene function
 * Rotates the scene and corrects its position.
 * Only valid for 90, -90 or 180 degree rotation
 * Prerequisite:  everything in the scene that should be rotated must ultimately have the saem scene root entity
 * and that root entity must be at the southwest corner of the southwest-most parcel.
 * @param sceneRoot the root entity of the scene from which all else is parented.  e.g. _scene in a Builder scene
 * @param degrees  90, -90 or 180 (as seen from the top, e.g. 90 is 90 degrees clockwise)
 * @param ewParcels how many parcels wide is the scene, east-west
 * @param nsParcels how many parcels deep is the scene, north-south
 * @returns void
 */
function rotateScene(sceneRoot:Entity, degrees:number, ewParcels:number, nsParcels:number ) {
  let transformToChange = sceneRoot.getComponent(Transform)
  switch (degrees) {
    case 90:
      transformToChange.position.z += 16 * ewParcels  // now move it back north by its original east-west size in meters
      break
    case -90:
      transformToChange.position.x += 16 * nsParcels  // now move it back east by its original south-north size in meters
      break
    case 180:
      transformToChange.position.x += 16 * ewParcels  // now move it back east by its original east-west size in meters
      transformToChange.position.z += 16 * nsParcels  // now move it back north by its original south-north size in meters
      break
    default:
      log("\n**** rotateBuilderScene error: invalid degrees")
      return
  }
  transformToChange.rotate(Vector3.Up(),degrees)
}

// rotateScene(_scene,90,2,2) // as an example of rotating a 2x2 parcel Builder scene 90 degrees clockwise.

```