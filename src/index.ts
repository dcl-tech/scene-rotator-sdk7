import { engine, Entity, Transform, MeshRenderer, MeshCollider } from '@dcl/sdk/ecs'
import { Vector3, Quaternion } from '@dcl/sdk/math'


// an example of a _scene Entity that is the parent of other Entities in the scene
let scene = engine.addEntity()
Transform.create(scene,{position:Vector3.create(0,0,0)})

// Call this function on an Entity that is the parent of all that needs to be rotated.
/**
 * Rotates a scene 90, -90, or 180 degrees around the vertical (y) axis, and restores the position of the scene
 * Only works for a square scene (ewParcels == nsParcels)
 * @param scene     // parent entity of all in the scene that should be rotatated.
 * @param degrees   // must be 90, -90, or 180.  Other values are ignored and have no effect.
 * @param parcvelWidth // number of parcels from west to east in your scene, which must be same as north to south.
 */
function rotateScene(scene:Entity, degrees:number, parcelWidth:number) {
  if (degrees != 90 && degrees != -90 && degrees != 180) {
    return // do nothing
  }
  if (parcelWidth < 1 ) {
    return
  }
  let mutableTransform = Transform.getMutable(scene)
  mutableTransform.rotation = Quaternion.multiply(
    mutableTransform.rotation,
    Quaternion.fromAngleAxis(degrees, Vector3.Up())
  )
  switch (degrees) {
    case 90:
      mutableTransform.position.z += 16*parcelWidth
      break
    case -90:
      mutableTransform.position.x += 16*parcelWidth 
      break
    case 180:
      mutableTransform.position.z += 16*parcelWidth
      mutableTransform.position.x += 16*parcelWidth 
      break
  }
}

export function main() {

  // for this example scene, we just have one box at 3,3,
  let boxEntity = engine.addEntity()
  Transform.create(boxEntity,
    {
      parent:scene,  // this makes the box to be parented by the scene entity
      position:Vector3.create(3,1,3)
    }
  )
  MeshRenderer.setBox(boxEntity)
  MeshCollider.setBox(boxEntity)

  // at some point, rotate the scene base entity
  rotateScene(scene, -90, 1) 
}
